# Arma Mod Loader 4

Automatic server, mod and modlist management with updating, cleanup and discord reports for ArmA dedicated servers. 
Written in pure powershell.

## Environment

The script makes some assumptions about how your servers are set up, namely: 
 - You're using a "basic" server management tool that does not already manage mods, i.e TADST, manual scripts or similar. (FAST / FASTER servers can be made to work, but you'll need to stop using faster for mod updates.)
 - You're using windows services to manage the servers themselves, In 501st we used to have FASTER run servers, but switched to using FASTER's export command option to run the servers using windows services, configured by FASTER. Using https://nssm.cc/ to create the services.
 - You're using HTML files as your source of modlist. It figures out what mods to load by reading a provided arma 3 launcher HTML file, it is not compatible with workshop collections.
 - It will export a -mod= argument for you to pass to your server manager of choice to configure the mods. In TADST this is the extra parameters box, otherwise I reccomend using a PAR file with the mod= option. https://community.bistudio.com/wiki/Startup_Parameters_Config_File
 For Example:

```
class Arg
{
	mod="-mod=@101st Doom Battalion Auxilliary Mod;@212th Auxiliary Assets;@327th AUX;@3AS (Beta Test);@3AS Terrains (Beta);@3den Enhanced;@501st Community Auxiliary Mod [Official];@91st MRC - Auxilliary Mod;@ace;@ACE 3 Extension (Animations and Actions);@ACE 3 Extension (Placeables);@ACE Pharmacy;@Advanced Rappelling;@Advanced Urban Rappelling;@BackpackOnChest;@Breaching Charge;@CamDetector;@CBA_A3;@CLA CLAFGHAN;@cTab;@CUP Terrains - Core;@CUP Terrains - CWA;@CUP Terrains - Maps;@Devourerking's Necroplague Mutants;@DUI - Squad Radar;@Eden Extended Objects;@Enhanced Movement;@EnhancedTrenches;@Freestyles Crash Landing;@G.O.S Al Rayak;@G.O.S Gunkizli;@G.O.S Leskovets;@G.O.S N'Djenahoud;@G.O.S N'ziwasogo;@G.O.S Song Bin Tahn;@Gruppe Adler Trenches;@Ihantala;@Ihantala Winter;@Isla Abramia;@Isla Duala;@Island Panthera;@Jbad;@Just Like The Simulations - The Great War;@Just Like The Simulations - The Great War - TFAR beta compatibility;@kerama Islands;@Kobra Mod Pack - Main;@LAMBS_Danger.fsm;@Last Force Project;@Legion - Battlefields;@Legion Base - Stable;@Legion Base Task Force Arrowhead Radio BETA Compatibility patch;@Legion Studios - ACE Compatibility patch;@LingorDingor Island;@LYTHIUM;@MBG Buildings Killhouses (Arma3 Remaster);@Montella Terrain Mod;@Mountain Wilderness Terrain;@MRH Satellite;@Nam;@Operation TREBUCHET;@Operation TREBUCHET ACE Compat;@Pandora;@Pulau;@Remove stamina;@Remove stamina - ACE 3;@Ruha;@Saint Kapaulio;@Task Force Arrowhead Radio (BETA!!!);@Tembelan Island;@UMB Colombia;@Uzbin Valley;@VIDDA  LEGACY VERSION;@VT5 - Valtatie 5  Final Release  Finnish terrain for A3;@WebKnight Droids;@Yellowstone;@ZEC - Zeus and Eden Templates  Building Compositions;@ZECCUP - Zeus and Eden Templates for CUP Terrains;@Zeus Enhanced;@Zeus Enhanced - ACE3 Compatibility";
};
```

## Getting started
 - Place the two powershell files, AML4.ps1 and discord.psm1 in your servers root folder.
 - Create a folder called `modlists` in the same place, place any HTML files you wish to load in the folder. 
 - Open AML4.ps1, configure the config options at the top, make sure to set: 
    - A Steam API Key, used to get playercounts from the Steamworks API. To prevent the server from shutting down with players online.
        - (Get from here: https://steamcommunity.com/dev/apikey)
    - The public arma server IP(s) of all servers you're running from the install. 
    - The Windows Service names of the arma 3 servers
        - This program requires the arma servers run inside windows services to control starting and stopping, I Reccomend an application such as NSSM (https://nssm.cc/) to set these up.
    - The path to the folder with SteamCMD in it, with no trailing space at the end (I.E F:\SteamCMD)
    - A Steam account that owns arma 3's username and password, and that doesn't have SteamGuard.
        - It *will* work with the anonymous account, but you need to manually run the script at least once with an account that owns Arma 3 so that SteamCMD will let you download the workshop mods.
        - If using anonymous, leave the password as an empty string, `""`

 - With that set up, you can now manually run the script to download and update the server, if you want full automation, read on.
    - Please note that, since the Arma 3 server requires SymLinks, the powershell script needs administrator for the perms to create those symlinks.
    - If you're using windows server, you'll also need to disable IE Enhanced Security Configuration in Server Manager > Local Server, so that the script can reach the steam workshop page to check for updates.

## Discord
The bot is capable of posting to discord in both an end user friendly format and for full logging. This is (now) optional, to enable, supply one or both of the webhooks into the $WEBHOOK or $DEVWEBHOOK variables.
 - To set up user friendly discord posting, create a text file containing the message format you want, place a `{0}` where you want the recently updated mods to be inserted (see example file `TechSupportFormat.txt`), and make sure the $FORMAT config variable is set to this file.
 - Let the bot run, and it will post your message along side mod updates the next time a mod updates, it will also generate ModUpdates.json, which contains 2 weeks of history and a message ID.
 - (optional) Set the $BLACKLIST variable to a list of steam workshop mod id's for mods you don't want the bot to announce updates to, for example server side only mods (example provided are the 501st server settingds mods)
 - (optional) If you want the bot to edit a message instead of reposting (i.e to pin it), enable discord developer mode and right click the message to copy the ID, place the message id in the generated ModUpdates.json file in the `messageID` variable. 
 - (optional) Add a second, private webhook into $DEVWEBHOOK for the bot to post the entire powershell scripts log into discord, useful to monitor full automation.

## Full Automation
Fully automating the script is as simple as calling it inside a windows scheduler task, set to repeat. 
 - To do so, first open task scheduler, select Create Task on the right
 - Call it w/e you want, i.e `AML4`
 - Set `Run whether user is logged on or not` to true
 - Set `Run with highest privileges` to true
 - Switch to the Triggers Tab
 - Create a new trigger, select One time
 - Enable `Repeat task every: ` and set it to how often you want the script to update, I use 10 minutes.
 - Set `for a duration of:` to `Indefinitely`
 - press OK
 - goto the Actions Tab
 - Create a new Action, select `Start a program`
 - select the powershell exe, usually found in `C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe`
 - `Add Arguments`, put the full path to the `AML4.ps1` script, i.e `F:\Arma3\AML4.ps1`
 - `Start in` put the path the arma server and AML4.ps1 are located in, i.e `F:\Arma3\`
 - In Settings, make sure its set to `Do not start a new instance`
 - finally, press okay.

 Thats it! the script should now be automatically triggering and updating your server, posting to discord and making sure not to kill the server mid op :D

 Please contact Hobnob#0001 on discord if you need any help. 
