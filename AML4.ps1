param (
    [switch]$verbose = $false,
    [switch]$fake = $false
)
if($verbose -eq $true){$VerbosePreference = "Continue"}else{$VerbosePreference = "SilentlyContinue"}

##AML V4.5

#steam api key, used to get server player count
$API_KEY=""
#server ip and port
$SERVER="148.251.50.222:2302"
#windows service name of the arma server, used to stop and start the servers. 
$SERVICE="501st-A3-S1"
#folder that contains steamcmd.exe, no \ at the end. 
$STEAM_PATH="C:\S1\SteamCMD"
#if you're having connection failed issues, try logging in with an account that owns arma
#otherwise, just use anon.
$STEAM_USER="anonymous"
$STEAM_PASS=""

#public discord channel to manage formatted discord message to (leave empty to disable)
$WEBHOOK=""
#discord message in a text file, supports normal discord formatting, use {0} to insert the latest updated mods.
$FORMAT=".\TechSupportFormat.txt"
#List of mods to not announce updates for, i.e serverside only mods.
$BLACKLIST=@("3038260782","3038262791","3038263253","3038263926","3038264454","3038264797","3038265340","3038265607","3038265991","3038266328","3038266908","3038272411","3038273013")
#private discord channel webhook to post update logs to (leave empty to disable)
$DEVWEBHOOK=""
#public git repo containing a mpmissions folder, to enable automatic missionfile sync, leave blank to disable, only none(public) or ssh key auth methods are supported
$MISSIONREPO="https://github.com/501stLegionA3/501st-Missions.git"
$BRANCH="master"

$DEVDISCORD=$false
if( $DEVWEBOOK -ne "" -and $(Test-Path '.\discord.psm1'))
{
    Add-Type -AssemblyName System.Web
    Add-Type -AssemblyName System.Net.Http
    Import-Module '.\discord.psm1'
    $DEVDISCORD=$true
}

function Read-ModList
{
    param 
    (
        [Parameter(Position=0,Mandatory=$true)]
        [System.IO.FileSystemInfo] $File
    )
    $ModList = @{}
    $HTML = New-Object -ComObject "HTMLFile"
    $HTML.write([System.Text.Encoding]::Unicode.GetBytes((Get-Content -path $File.FullName -Raw)))

    $mods = $HTML.body.getElementsByTagName('tr')

    Foreach($mod in $mods)
    {
        $name = ""
        Foreach($row in $mod.getElementsByTagName("TD"))
        {
            if($row.getAttribute("data-type") -match "DisplayName")
            {
                $name = $row.innerText
                break
            }
        }
        $link = $mod.getElementsByTagName('a')[0].href
        if($link -match "id=(?<id>.*)")
        {
            $id = $Matches.id
            Write-Verbose("[$($File.name)] Mod: $name(id: $id) Found!")
            if($id -eq "682140680")
            {
                $ret = @{id = $id; name = "kerama Islands"}
            }else
            {
                $ret = @{id = $id; name = $name}
            }  
            $ModList.Add($id, $ret)
        }else
        {
            Write-Warning("[$($File.name)] Cannot find ID from URL for mod: $name, is it local?")
        }
    }
    $null = [Runtime.InteropServices.Marshal]::ReleaseComObject($HTML)
    return $ModList
}
Function Remove-InvalidFileNameChars 
{
    param(
      [Parameter(Mandatory=$true,
        Position=0,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true)]
      [String]$Name
    )
  
    $invalidChars = [IO.Path]::GetInvalidFileNameChars() -join ''
    $re = "[{0}]" -f [RegEx]::Escape($invalidChars)
    return ($Name -replace $re)
}
function GetRemoteUpdatedTime
{
    param([Parameter(Mandatory=$true,Position=0)][String]$ModID)
    Write-Verbose "Trying to get WebPage: https://steamcommunity.com/sharedfiles/filedetails/changelog/$ModID"
    $response = Invoke-WebRequest -useBasicParsing "https://steamcommunity.com/sharedfiles/filedetails/changelog/$ModID"
    $HTML = New-Object -ComObject "HTMLFile"
    $HTML.write(([System.Text.Encoding]::Unicode.GetBytes($response.Content)))
    #first workshop announcement is latest, first P tag contains the unix timestamp as the ID.
    if(!$HTML -Or !$HTML.getElementsByClassName("workshopAnnouncement")[0]){ Write-Verbose "Failed to load workshop page"; return New-Object System.DateTimeOffset}
    $unixTime = $HTML.getElementsByClassName("workshopAnnouncement")[0].getElementsByTagName("P")[0].id
    $null = [Runtime.InteropServices.Marshal]::ReleaseComObject($HTML)
    return [System.DateTimeOffset]::FromUnixTimeSeconds($unixTime)
}
function GetLocalUpdatedTime
{
    param([Parameter(Mandatory=$true,Position=0)][String]$ModID)
    if(Test-Path "$STEAM_PATH\steamapps\workshop\content\107410\$ModID")
    {
        $folder = Get-Item "$STEAM_PATH\steamapps\workshop\content\107410\$ModID"
        return [System.DateTimeOffset]$folder.LastWriteTime
    }
    else
    {
        return New-Object System.DateTimeOffset
    }
}
function GetArmaPlayerCount
{
    param([Parameter(Mandatory=$true,Position=0)][String]$ServerIP)
    $serverInfo = Invoke-WebRequest "https://api.steampowered.com/IGameServersService/GetServerList/v1/?filter=\appid\107410\gameaddr\$ServerIP\&key=$API_KEY" | ConvertFrom-Json
    #If the app id isnt arma 3's, assume some kind of failure and raise panic
	if($serverInfo.response.servers.appid -ne "107410"){ 
		Write-Error -Message "Critical Giggl in player count detection, Halting.." -Category ConnectionError
		Stop-Transcript
		if($DEVDISCORD){
			(Get-Content -raw aml4.ps1.log) `
			-replace "^\*+\r\n[\s\S]+?(?=\*).*\r\n", "[AML4 UPDATE LOG] FOR SERVICES: $SERVICE`n" `
			-replace "\*+\r\n[\s\S]+?(?=\*).*\r\n", "" | Out-File "$($MyInvocation.MyCommand.Path).log"
			Send-DiscordMessage -URL $DEVWEBHOOK -File "$($MyInvocation.MyCommand.Path).log"
		}
		exit 1
		return "1" 
	} else { return $serverInfo.response.servers.players }
}
function GetGitUpdateNeeded
{
    #if a repo is configured
    if($MISSIONREPO -ne ""){
        #if the git repo is initalized
        if(Test-Path ".\.git"){
            #make sure remote is set
            if($(git config --get remote.origin.url) -ne $MISSIONREPO){
                Write-Output "git remote not set, fixing"
                git remote add origin $MISSIONREPO  | Out-Null
                git remote set-url origin $MISSIONREPO | Out-Null
            }
        } else {
            #set up repo
            git init | Out-Null
            git remote add origin $MISSIONREPO | Out-Null
            git config core.sparsecheckout true | Out-Null
            git sparse-checkout set /mpmissions/ | Out-Null
            git fetch | Out-Null
			git pull origin $BRANCH | Out-Null
            git checkout $BRANCH | Out-Null
			git branch --set-upstream-to=origin/$BRANCH | Out-Null
        }
        git remote update | Out-Null
        return ($(git rev-parse "@") -ne $(git rev-parse "@{u}") 2> $null)
    }
    return $false
}

Start-Transcript -Force -Path "$($MyInvocation.MyCommand.Path).log"

#always assume the server is currently running a campaign op at any given moment
$WasRunning = $true

#skip check if service is off
if ( (Get-Service $SERVICE).Status -eq "Stopped" )
{ 
    $WasRunning = $false
    Write-Output "SERVICE STOPPED, SKIPPING PLAYER CHECK."
}
else
{
    #Start by checking for players online, so that we dont spend server resources checking for updates while players are online
    $TotalPlayersOnline = 0
    $Players = GetArmaPlayerCount $SERVER
    $TotalPlayersOnline = $TotalPlayersOnline + $Players
    Write-Verbose "Server: $SERVER : $Players"
    Write-Output "There are currently: $TotalPlayersOnline Players online across all servers"
    if($TotalPlayersOnline -gt 0) { Write-Output "Cannot update because servers are in use, Exiting."; Exit }
}



#Next, build a list of all mods from the installed modlist html files
$Mods = @{}
$ModLists = Get-ChildItem .\modlists\ | Where-Object {$_.Extension -eq ".html"}
$ModsToUpdate = @{}

Foreach($Modlist in $ModLists)
{
    $ret = Read-ModList -File $ModList
    Foreach($mod in $ret.GetEnumerator())
    {
        if(-not $Mods.ContainsKey($mod.Value.id))
        {
            $Mods.Add($mod.Value.id, $mod.Value)
        }
    }
}

if($fake -eq $true)
{
    write-output "Faking mod update, skipping mod and git checks"
}
else 
{
    #Now check for updates
    $CurrentModUpdates = @()
    Foreach($Mod in $Mods.GetEnumerator())
    {
        $LocalTime = GetLocalUpdatedTime $Mod.Value.id
        $RemoteTime = GetRemoteUpdatedTime $Mod.Value.id
        $Delta = $RemoteTime - $LocalTime
        
        if($LocalTime -lt $RemoteTime)
        {
            Write-Output "Outdated Mod Detected!`n`"$($Mod.Value.name)`" is $($Delta.Days) Days, $($Delta.Hours) Hours, $($Delta.Minutes) Minutes and $($Delta.Hours) Seconds out of date!"
            $ModsToUpdate.Add($Mod.Value.id, $Mod.Value)
            #if the local time year is less than 2012 (arma3 release year), that means we couldn't find the local files, probably a new mod, dont announce it as an update.
            #also make sure the mods not on the blacklist
            if($LocalTime.Year -gt 2012 -and !$($BLACKLIST -contains $Mod.Value.id))
            {
                $CurrentModUpdates = @(
                    [PSCustomObject]@{time="$($RemoteTime.ToUnixTimeSeconds())";"name"=$Mod.Value.name}
                    $CurrentModUpdates
                )
            }
        }
    }

    #check for missionfile updates
    $GitUpdateNeeded = GetGitUpdateNeeded

    if($ModsToUpdate.Count -eq 0 -and !$GitUpdateNeeded){ Write-Output "No Updates found, Exiting."; Exit }

    Write-Output "$($ModsToUpdate.Count) Update(s) Found! Updating!"
    Write-Output "Git Update Needed: $GitUpdateNeeded"
}

#Stop Server
Write-Verbose "Stopping Server: $SERVICE"; Stop-Service $SERVICE 

#Remove Old SymLinks
Foreach ($SymLink in $(Get-ChildItem | Where-Object { $_.Attributes -match "ReparsePoint"}))
{
    Write-Verbose("Removing Old SymLink $($SymLink.Name)")
    $SymLink.Delete()
}

#Delete any Unused Mods
Foreach ($ExistingMod in $(Get-ChildItem "$STEAM_PATH\steamapps\workshop\content\107410\"))
{
    if($Mods.ContainsKey($ExistingMod.Name))
    {
        Write-Verbose("Mod Still Valid! $ExistingMod")
    }else {
        Write-Verbose("Mod Invalid! Deleting! $ExistingMod")
        Remove-item $ExistingMod.FullName -Recurse -Force -Confirm:$false
    }
}

#Download any mod updates using SteamCMD
$SteamCMDCommand = "$STEAM_PATH\steamcmd.exe +login '$STEAM_USER' '$STEAM_PASS' "
Foreach ($Mod in $ModsToUpdate.GetEnumerator())
{
    $SteamCMDCommand = $SteamCMDCommand + "+workshop_download_item 107410 $($Mod.Value.id) "
}
if( $fake -eq $true ) { Write-Verbose "Skipping SteamCMD invoke because -fake flag passed."}
else
{
    Write-Verbose("Running Workshop Command '$SteamCMDCommand +quit'")
    Invoke-Expression "$SteamCMDCommand +quit"
}

#Create SymLinks.
Foreach ($Mod in $Mods.GetEnumerator())
{
    $source = "$STEAM_PATH\steamapps\workshop\content\107410\$($Mod.Value.id)"
    $destination = ".\@$(Remove-InvalidFileNameChars -Name $Mod.Value.name)"
    Write-Verbose("Creating SymLink: $source > $destination")
    $null = New-Item -ItemType SymbolicLink -Path $destination -Target $source -Force
}

<# DEPRECATED in favour of parfile.
#Output -mod argument
Foreach($Modlist in $ModLists)
{
    $ModCommand = '-mod="'
    $Sorted = $(Read-ModList -File $ModList).GetEnumerator() | Sort-Object { $_.Value.name }
    if($Sorted.Count -eq 1)
    {
        $Mod = $Sorted[0]
        $name = $(Remove-InvalidFileNameChars -Name $Mod.Value.name)
        $ModCommand = $ModCommand + "@$name;"
    }else{
    Foreach($mod in $Sorted.GetEnumerator())
    {
        $name = $(Remove-InvalidFileNameChars -Name $Mod.Value.name)
        $ModCommand = $ModCommand + "@$name;"
        }
    }
    $ModCommand = $ModCommand + '"'
    Write-Output("$($Modlist.Name) Command: ")
    Write-Output($ModCommand)
    Write-Output("")
}
#>
#parfile stuff
$ParFile = @"
class Arg
{
    mod="-mod=
"@

$Sorted = $Mods.GetEnumerator() | Sort-Object { $_.Value.name }

if($Sorted.Count -eq 1)
{
    $Mod = $Sorted[0]
    $name = $(Remove-InvalidFileNameChars -Name $Mod.Value.name)
    $ParFile = $ParFile + "@$name;"
}else{
Foreach($mod in $Sorted.GetEnumerator())
{
    $name = $(Remove-InvalidFileNameChars -Name $Mod.Value.name)
    $ParFile = $ParFile + "@$name;"
    }
}

$ParFile = ($ParFile -replace ';$') + @"
";
};
"@

Write-Output "Par File Updated"
Write-Verbose $ParFile
$ParFile | Out-File ".\mods.par"

#download any missionfile updates
if( $GitUpdateNeeded ) 
{ 
    git fetch --all
    git reset --hard origin/$BRANCH
}

#Start Server (or don't if it wasnt started in the first place)
if ( $WasRunning ) { Write-Verbose "Starting Server: $SERVICE"; Start-Service $SERVICE }

Write-Output "================================`n`nUpdate Complete! $($ModsToUpdate.Count) Mod(s) Updated!"

#Post user-friendly tech support message
if($WEBHOOK -ne "")
{
    $format = [string](Get-Content $FORMAT -Raw)
    $modUpdates = [string](Get-Content "ModUpdates.json" -Raw) | ConvertFrom-Json
    $modUpdates.updates = @(
        $CurrentModUpdates
        $modUpdates.updates | Where-Object -Property time -GT ([DateTimeOffset](Get-Date).AddDays(-14)).ToUnixTimeSeconds()
    )
	$modUpdates.updates = $modUpdates.updates = $modUpdates.updates | Group-Object -Property name |
    Select-Object name, @{ Name = 'time'; Expression = { $_.Group.time | Sort-Object -Descending | Select-Object -First 1 } }
	$modUpdates | ConvertTo-Json | Set-Content ".\ModUpdates.json"
    [string]$modUpdateString = [string]""
    
    foreach($modUpdate in $modUpdates.updates)
    {
        $modUpdateString += [string]"<t:$($modUpdate.time):f> - $($modUpdate.name)`n"
    }
    $payload = @{flags=4; content = [string]$format -f [string]$modUpdateString} | ConvertTo-Json
    if($modUpdates.messageID -ne "")
    {
        Invoke-RestMethod -Uri "$WEBHOOK/messages/$($modUpdates.messageID)" -Method Patch -ContentType 'Application/Json' -Body $payload
    } else 
    {
        Invoke-RestMethod -Uri $WEBHOOK -Method Post -ContentType 'Application/Json' -Body $payload
    }
}
Stop-Transcript
if($DEVDISCORD){
    (Get-Content -raw aml4.ps1.log) `
    -replace "^\*+\r\n[\s\S]+?(?=\*).*\r\n", "[AML4 UPDATE LOG] FOR SERVICE: $SERVICE`n" `
    -replace "\*+\r\n[\s\S]+?(?=\*).*\r\n", "" | Out-File "$($MyInvocation.MyCommand.Path).log"
    Send-DiscordMessage -URL $DEVWEBHOOK -File "$($MyInvocation.MyCommand.Path).log"
}
